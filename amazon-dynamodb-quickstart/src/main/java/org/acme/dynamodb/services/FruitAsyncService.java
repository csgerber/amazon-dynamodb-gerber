package org.acme.dynamodb.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import io.smallrye.mutiny.Uni;
import org.acme.dynamodb.model.Fruit;
import org.acme.dynamodb.repos.FruitAsyncRepo;
import software.amazon.awssdk.services.dynamodb.DynamoDbAsyncClient;

@ApplicationScoped
public class FruitAsyncService  {

    @Inject
    FruitAsyncRepo fruitAsyncRepo;

    public Uni<List<Fruit>> findAll() {
        return fruitAsyncRepo.findAll();
    }

    public Uni<List<Fruit>> add(Fruit fruit) {
        return fruitAsyncRepo.add(fruit);
    }

    public Uni<Fruit> get(String name) {
        return fruitAsyncRepo.get(name);
    }
}
