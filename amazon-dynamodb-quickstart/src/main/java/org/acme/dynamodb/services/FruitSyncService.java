package org.acme.dynamodb.services;

import java.util.List;
import java.util.stream.Collectors;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

import org.acme.dynamodb.model.Fruit;
import org.acme.dynamodb.repos.FruitSyncRepo;
import software.amazon.awssdk.services.dynamodb.DynamoDbClient;

@ApplicationScoped
public class FruitSyncService  {

    @Inject
    FruitSyncRepo fruitSyncRepo;

    public List<Fruit> findAll() {
      return   fruitSyncRepo.findAll();
    }

    public List<Fruit> add(Fruit fruit) {
      return  fruitSyncRepo.add(fruit);
    }

    public Fruit get(String name) {
        return fruitSyncRepo.get(name);
    }
}